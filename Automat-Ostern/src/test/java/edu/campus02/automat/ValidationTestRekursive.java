package edu.campus02.automat;

import static org.junit.Assert.*;

import org.junit.Test;

public class ValidationTestRekursive {
	@Test
	public void test01Valid() throws Exception {

		assertTrue(ValidateUtil.verifyRekursive(CreationUtil.create01(), "aac"));
		assertTrue(ValidateUtil.verifyRekursive(CreationUtil.create01(), "ac"));
		assertTrue(ValidateUtil.verifyRekursive(CreationUtil.create01(), "bc"));

	}

	@Test
	public void test01InvalidNoTransition() throws Exception {

		assertFalse(ValidateUtil.verifyRekursive(CreationUtil.create01(), "ad"));
	}

	@Test
	public void test01InvalidNotFinal() throws Exception {

		assertFalse(ValidateUtil.verifyRekursive(CreationUtil.create01(), "aa"));
	}

	@Test
	public void test02InvalidNotFinal() throws Exception {

		assertFalse(ValidateUtil.verifyRekursive(CreationUtil.create02(), "ab"));
	}

	@Test
	public void test02Final() throws Exception {

		assertTrue(ValidateUtil.verifyRekursive(CreationUtil.create02(), "abc"));
		assertTrue(ValidateUtil.verifyRekursive(CreationUtil.create02(), "aabbbc"));
	}

	@Test
	public void test02InvalidNotTransition() throws Exception {

		assertFalse(ValidateUtil.verifyRekursive(CreationUtil.create02(), "ax"));
	}

}
