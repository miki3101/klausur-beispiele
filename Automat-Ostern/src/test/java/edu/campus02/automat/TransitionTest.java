package edu.campus02.automat;

import org.junit.Test;
import static org.junit.Assert.*;

public class TransitionTest {


	@Test
	public void testConstructor01() {
		Transition t = new Transition("ab", null);
		assertEquals("[ab]->null",t.toString());
	}
	
	@Test
	public void testConstructor02() {
		Transition t = new Transition("ab", new State("s1"));
		assertEquals("[ab]->(s1,false)",t.toString());
	}
	
	@Test
	public void testMatchingTransition() {
		Transition t = new Transition("ab", null);
		assertTrue(t.match('a'));
	}

	@Test
	public void testNotMatchingTransition() {
		Transition t = new Transition("ab", null);
		assertFalse(t.match('c'));
	}

	@Test
	public void testGetState() {
		State target = new State("1");
		Transition t = new Transition("ab", target);
		assertEquals(target, t.getTarget());
	}

	@Test
	public void testToString() {
		State target = new State("1");
		Transition t = new Transition("ab", target);
		assertEquals("[ab]->(1,false)", t.toString());
	}

}
