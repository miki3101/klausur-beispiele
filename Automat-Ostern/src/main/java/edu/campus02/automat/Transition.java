package edu.campus02.automat;

public class Transition {
	
	private String trigger;
	private State target = new State("1", false);
	

	public Transition(String trigger, State target) {
		
		this.trigger = trigger;
		this.target = target;
		
	}
	
	public State getState()
	{
		
		return target;
	}

	public State getTarget() {
		return target;
	}

	public boolean match(char c) {
		
		char[] test = trigger.toCharArray();
		
		for (char d : test)
		{
			if(d == c)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public String toString()
	{
		return "[" + trigger + "]"+ "->" + target;
	}

}
