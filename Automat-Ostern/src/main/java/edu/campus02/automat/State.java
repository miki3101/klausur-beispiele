package edu.campus02.automat;

import java.util.ArrayList;

public class State {
	
	private String name;
	private boolean fina;
	
	private ArrayList<Transition> uebergang = new ArrayList<Transition>();
	
	public State(String name) {
		
		this.name = name;
	}

	public State(String name, boolean fina) {
		
		this.name = name;
		this.fina = fina;
	}

	public boolean isFinal() {
	
		return fina;
	}

	public void addTransition(Transition tran) {
		
		uebergang.add(tran);

	}

	public Transition findMatchingTransition(char c) {
		
		Transition gefunden = null;
		
		for (Transition transition : uebergang)
		{
			if(transition.match(c))
			{
				gefunden = transition;
			}
		}
		
		return gefunden;
	}
	
	public String toString()
	{
		if(uebergang.isEmpty())
		{
		
		return String.format("(%s,%b)", name, fina);
		}
		return String.format("(%s,%b,%s)", name, fina, uebergang.toString());
	}

}
