package edu.campus02.easter;

import java.util.ArrayList;

public class Bag {
	
	ArrayList<Egg> beutel = new ArrayList<>();

	public void putEgg(Egg egg) {
		
		beutel.add(egg);

	}

	public int countEggs() {
		return beutel.size();
	}

	public double sumWeight() {
		
		double summe = 0;
		
		for (Egg egg : beutel)
		{
			summe += egg.getWeight();
		}
		return summe;
	}

	public double sumWeight(String color) {
		
		double summefarbe = 0;
		
		for (Egg egg : beutel)
		{
			if(egg.getColor() == color)
			{
			summefarbe += egg.getWeight();
			}
		}

		return summefarbe;
	}

	public int countDifferentColors() {
		
		int farbenanzahl = 0;
		ArrayList<String> farben = new ArrayList<>();
		
		for (Egg egg : beutel)
		{
			if(!farben.contains(egg.getColor()))
			{
				farben.add(egg.getColor());
			}
		}

		return farben.size();
	}

}
