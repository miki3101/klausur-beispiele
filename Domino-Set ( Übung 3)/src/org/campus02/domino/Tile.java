package org.campus02.domino;

public class Tile {
	
	 private int number1;
	 private int number2;

	public Tile(int h, int s) {
		
		if(h>s)
		{
			this.number1 = h;
			this.number2 = s;
		}
		else if(s>h)
		{
			this.number1 = s;
			this.number2 = h;
		}
	}

	public int getNumber1() {
		return number1;
	}

	public int getNumber2() {
		return number2;
	}

	public boolean compare(Tile otherTile) {
		
		if(otherTile.getNumber1() > this.getNumber1())
		{
			return true;
		}
		else if (otherTile.getNumber1() == this.getNumber1() && otherTile.getNumber2() > this.getNumber2())
		{
			return true;
		}
	

		return false;
	}

	
	public String toString() {
		
		return String.format("(%d,%d)", number1, number2);
	}
}
