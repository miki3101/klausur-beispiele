package org.campus02.domino.util;

import java.util.ArrayList;

import org.campus02.domino.Tile;

public class TileUtil {
	

	public static int sum(ArrayList<Tile> tiles) {
		
		int summe = 0;
		
		for (Tile tile : tiles)
		{
			summe += tile.getNumber1() + tile.getNumber2();
		}
		
		return summe;
	}

	public static Tile greatesTile(ArrayList<Tile> tiles) {
		
		Tile groesste = tiles.get(0);
		
		for (Tile tile : tiles)
		{
			if(groesste.compare(tile))
			{
				groesste = tile;
			}
		}
		
			
			return groesste;

	}
}
