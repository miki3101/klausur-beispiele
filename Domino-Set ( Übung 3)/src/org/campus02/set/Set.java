package org.campus02.set;

import java.util.ArrayList;

public class Set {
	
	ArrayList<Integer> menge = new ArrayList<Integer>();
	ArrayList<Set> sets = new ArrayList<>();

	public void add(int value) {
		
		menge.add(value);
		
	}

	public void add(Set set) {
		
		sets.add(set);

	}

	public int sum() {
		
		int summe = 0;
		
		for (Integer integer : menge)
		{
			summe += integer;
		}
		
		for (Set zahlen : sets)
		{
			summe+= zahlen.sum();
		}
		
		return summe;
	}
	
	public String toString()
	{
		if (sets.isEmpty())
		{
			return menge.toString();
		}
		return menge.toString()+ " " + sets.toString();
	}

}
