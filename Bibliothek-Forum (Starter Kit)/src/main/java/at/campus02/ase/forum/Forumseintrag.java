package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag {
	
	private String titel;
	private String text;
	
	private ArrayList<Forumseintrag> antworten = new ArrayList<>();

	public Forumseintrag(String titel, String text) {
		
		this.titel = titel;
		this.text = text;
	}

	public Forumseintrag antworten(String titel, String text) {
		
		Forumseintrag neu = new Forumseintrag(titel, text);
		
		antworten.add(neu);
		
		return neu;
	}

	public ArrayList<Forumseintrag> getAntworten() {
		return antworten;
	}

	public int anzahlDerEintraege() {
		
		int anzahl = 0;
		
		for (Forumseintrag forumseintrag : antworten)
		{
			anzahl += 1 + forumseintrag.anzahlDerEintraege();
		}

		return anzahl;
	}
	
	public String toString()
	{
		if(antworten.isEmpty())
		{
		return String.format("(%s,%s)", titel, text);
		}
		return String.format("(%s,%s)%s", titel, text, antworten.toString());
	}
}
