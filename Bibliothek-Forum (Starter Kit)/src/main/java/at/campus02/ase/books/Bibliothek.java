package at.campus02.ase.books;

import java.util.ArrayList;

public class Bibliothek {
	
	ArrayList<Book> buecher = new ArrayList<>();

	public void addBook(Book b) {
		
		buecher.add(b);
	}

	public int countPages() {
		
		int seiten = 0;
		
		for (Book book : buecher)
		{
			seiten += book.getSeiten();
		}
		return seiten;
	}

	public double avaragePages() {
		
		double durchschnitt = 0;
		
		for (Book book : buecher)
		{
			durchschnitt += book.getSeiten();
		}
		
		return durchschnitt/buecher.size();
	}

	public ArrayList<Book> booksByAuthor(String autor) {
		
		ArrayList<Book> autoren = new ArrayList<>();
		
		for (Book book : buecher)
		{
			if(autor == null)
			{
				return autoren;
			}
			else if (book.getAutor() == autor)
			{
				autoren.add(book);
			}
		}
		
		return autoren;
	}

	public ArrayList<Book> findBook(String search) {
		
		ArrayList<Book> ergebnis = new ArrayList<>();
		
		for (Book book : buecher)
		{
			if(search == null)
			{
				return ergebnis;
			}
			else if(book.getAutor().contains(search) || book.getTitel().contains(search))
			{
				ergebnis.add(book);
			}
		}

		return ergebnis;
	}

}
