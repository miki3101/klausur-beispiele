import at.campus02.ase.books.Bibliothek;
import at.campus02.ase.books.Book;

public class BookApp
{

	public static void main(String[] args)
	{
		Bibliothek test = new Bibliothek();
		
		Book a = new Book("Alex", "geht nach Hause", 100);
		Book b = new Book("Susi", "geht nach Hause", 200);
		Book c = new Book("Max", "geht nach Hause", 300);
		
		test.addBook(a);
		test.addBook(b);
		test.addBook(c);
		
		System.out.println(test.countPages());

	}

}
