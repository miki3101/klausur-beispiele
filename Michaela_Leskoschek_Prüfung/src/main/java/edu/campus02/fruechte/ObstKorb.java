package edu.campus02.fruechte;

import java.util.ArrayList;

public class ObstKorb
{

	private ArrayList<Frucht> korb = new ArrayList<>();

	public void fruchtAblegen(Frucht f)
	{

		korb.add(f);
	}

	public int zaehleFruechte(String sorte) {
		
		int fruechte = 0;
		
		for (Frucht frucht : korb)
		{
			if(frucht.getSorte() == sorte)
			{
				fruechte++;
			}
		}

		return fruechte;
	}

	public Frucht schwersteFrucht()
	{

		Frucht schwerste = korb.get(0);

		for (Frucht frucht : korb)
		{
			if (frucht.getGewicht() > schwerste.getGewicht())
			{
				schwerste = frucht;
			}
		}

		return schwerste;
	}

	public ArrayList<Frucht> fruechteSchwererAls(double gewicht)
	{

		ArrayList<Frucht> schwere = new ArrayList<>();

		for (Frucht frucht : korb)
		{
			if (frucht.getGewicht() > gewicht)
			{
				schwere.add(frucht);
			}
		}

		return schwere;
	}

	public int zaehleSorte()
	{
		ArrayList<String> sorten = new ArrayList<>();
		
		for (Frucht frucht : korb)
		{
			if(!sorten.contains(frucht.getSorte()))
			{
				sorten.add(frucht.getSorte());
			}
		}
		

		return sorten.size();
	}

	public String toString()
	{
		return korb.toString();
	}
}
