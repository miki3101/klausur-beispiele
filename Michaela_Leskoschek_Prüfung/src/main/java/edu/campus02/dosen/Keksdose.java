package edu.campus02.dosen;

import java.util.ArrayList;

public class Keksdose {
	
	private String farbe;
	private double breite;
	private double laenge;
	private double hoehe;
	
	private Keksdose innereDose;
	
	private ArrayList<Keks> dose = new ArrayList<>();
	private ArrayList<Keksdose> doseInDose = new ArrayList<>();

	public Keksdose(String farbe, double x, double y, double hoehe) {
		
		this.farbe = farbe;
		this.hoehe = hoehe;
		
		if(x<y)
		{
			this.breite = x;
			this.laenge = y;
		}
		else
		{
			this.breite = y;
			this.laenge = x;
		}
		
	}

	public double getBreite() {
		return breite;
	}

	public double getLaenge() {
		return laenge;
	}

	public double getHoehe() {
		return hoehe;
	}

	public double volumen() {
		
		double volumen = 0;
		
		volumen = breite * laenge * hoehe;
		
		return volumen;
	}

	public double freiesVolumen() {
		
		double freiesV = this.volumen();
		
		for (Keks keks : dose)
		{
			freiesV -= keks.getVolumen();
	
		}
		
		return freiesV;
	}

	public boolean keksAblegen(Keks keks) {
		
		if(this.freiesVolumen() >= keks.getVolumen())
		{
			dose.add(keks);
			
			return true;
		}
		
		return false;
		
	}

	public boolean leer() {
		
		if(dose.isEmpty())
		{
			return true;
		}
		return false;
	}

	public boolean stapeln(Keksdose d) {
		
		if(this.leer() && this.getBreite()> d.getBreite() && this.getHoehe() > d.getHoehe() && this.getLaenge() > d.getLaenge())
		{
			innereDose = d;
			innereDose.stapeln(d);
			return true;
		}

		return false;
	}

	public double gesamtVolumen() {
		
		double gesamt = this.volumen();
		
		if(this.stapeln(this))
		{
			gesamt += innereDose.gesamtVolumen();
		}
		
		
		return gesamt;
	}
	
	public String toString()
	{
		if(doseInDose.isEmpty())
		{
		return String.format("(%s %.1f x %.1f x %.1f)", farbe, breite, laenge, hoehe);
		}
		return String.format("(%s %.1f x %.1f x %.1f %s)", farbe, breite, laenge, hoehe, doseInDose.toString());
	}
}
