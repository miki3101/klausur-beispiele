package edu.campus02.dosen.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.campus02.dosen.Keks;

public class KeksTest {

	@Test
	public void constructorTest() throws Exception {
		Keks k = new Keks("Lebkuchen", 20, 40);
		
		assertEquals(20, k.getGewicht(), 0.01);
		assertEquals(40, k.getVolumen(), 0.01);
		assertEquals("Lebkuchen", k.getName());
	}
}
