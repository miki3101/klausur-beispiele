import at.campus02.ase.filesystem.File;
import at.campus02.ase.filesystem.Filesystem;

public class FilesystemApp
{

	public static void main(String[] args)
	{
		Filesystem test = new Filesystem();
		
		File a = new File("Alex", "geht nach Hause", 100);
		File b = new File("Susi", "geht nach Hause", 200);
		File c = new File("Max", "geht nach Hause", 300);
		
		test.addFile(a);
		test.addFile(b);
		test.addFile(c);
		
		System.out.println(test);
		System.out.println(test.countSize());

	}

}
