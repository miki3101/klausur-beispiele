package at.campus02.ase.messages;

import java.util.ArrayList;

public class Message
{
	
	private String titel;
	private String text;
	
	private ArrayList<Message> liste = new ArrayList<Message>();

	public Message(String titel, String text)
	{
		this.titel = titel;
		this.text = text;
	}

	public Message antworten(String titel, String text)
	{
		Message antworten = new Message(titel, text);
		
		liste.add(antworten);

		return antworten;
	}

	public ArrayList<Message> getAntworten()
	{
		return liste;
	}

	public int countMessages()
	{
		int gesamtantworten = 0;
		
		for (Message message : liste)
		{
			gesamtantworten += 1 + message.countMessages();
		}

		return gesamtantworten;
	}
	
	public String toString()
	{
		if(liste.isEmpty())
		{
			return String.format("(%s,%s)", titel, text);
		}
		else
			return String.format("(%s,%s)%s", titel, text, liste.toString());
	}
	
	

}
