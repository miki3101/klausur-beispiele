package at.campus02.ase.filesystem;

import java.util.ArrayList;

public class Filesystem
{
	
	ArrayList<File> bibliothek = new ArrayList<File>();

	public void addFile(File b)
	{
		bibliothek.add(b);
	}

	public int countSize()
	{
		int seiten = 0;
		
		for (File file : bibliothek)
		{
			seiten += file.getSize();
		}

		return seiten;
	}

	public double averageSize()
	{
		double durchschnitt = 0;
		
		for (File file : bibliothek)
		{
			durchschnitt += file.getSize();
		}

		return durchschnitt/bibliothek.size();

	}

	public ArrayList<File> fileByOwner(String owner)
	{
		ArrayList<File> autor = new ArrayList<File>();
		
		for (File file : bibliothek)
		{
			if(owner == null)
			{
				return autor;
			}
			else if (file.getOwner() == owner)
			{
				autor.add(file);
			}
		}

		return autor;
	}

	public ArrayList<File> findFile(String search)
	{
		ArrayList<File> finden = new ArrayList<File>();
		
		for (File file : bibliothek)
		{
			if(search == null)
			{
				return finden;
			}
			else if(file.match(search))
			{
				finden.add(file);
			}
		}

		return finden;
	}
	
	public String toString()
	{
		return bibliothek.toString();
	}
}
